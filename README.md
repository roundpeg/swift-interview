# Swift Interview Project

## Basic Login Dialog

### User's First Visit

Here is what the user's first visit experience should be...

* user launches app and is presented with login dialog (userName, password, login button)
* user signs in (any user name / password )
* app validates that the user name passes minimum validation (should look like an email address and be between 6 and 50 characters)
* app validates that the password passes minimum validation (4 to 40 chars)
* login service is invoked with a callback to get either the user or and error
* if login is successful, the user's session key is saved to user settings and the app continues
* if an error is detected, the login should be rejected with a minimal message
* the user exits the application (still logged in)

### Repeat Visit

Upon repeat visits the user's session should be retrieved from user settings.  The user experience would be...

* the user re-enters the application and sees the their name and a logout button
* the user logs out (killing the user settings)
* user is presented the login/password/login button dialog once again

- - -
darryl.west@roundpeg.com | Version 2015.06.04-20