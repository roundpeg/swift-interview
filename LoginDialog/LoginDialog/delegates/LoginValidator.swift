//
//  LoginValidator.swift
//  LoginDialog
//
//  Created by darryl west on 6/29/15.
//  Copyright © 2015 darryl west. All rights reserved.
//

import Foundation

public struct MinMaxRange {
    public let min:Int
    public let max:Int
    
    public func isValid(value:Int) -> Bool {
        return (value >= self.min && value <= self.max)
    }
    
    public func isValid(value:String) -> Bool {
        return isValid( value.characters.count )
    }
    
    public init(min:Int, max:Int) {
        self.min = min
        self.max = max
    }
}

public class LoginValidator {
    let log:Logger
    let usernameRange:MinMaxRange
    let passwordRange:MinMaxRange
    
    public func isUsernameValid(username:String) -> Bool {
        if usernameRange.isValid( username ) {
            // TODO: verify that the user name looks like an email
            
            log.info("user name is valid: \( username )")
            
            return true
        }
        
        log.warn("user name is invalid: \( username )")
        
        return false
    }
    
    public func isPasswordValid(password:String) -> Bool {
        // just validate the length
        return passwordRange.isValid( password )
    }
    
    init(configuration:Configuration) {
        self.log = configuration.createLogger("LoginValidator")
        
        self.usernameRange = configuration.usernameLengthRange
        self.passwordRange = configuration.passwordLengthRange
        
        // TODO add other configured rules
    }
}
