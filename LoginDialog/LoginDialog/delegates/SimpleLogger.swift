//
//  SimpleLogger.swift
//  LoginDialog
//
//  Created by darryl west on 6/29/15.
//  Copyright © 2015 darryl west. All rights reserved.
//

import Foundation

public protocol LogAppenderType {
    var name:String { get }
    
    func format(category cat:String, levelName name:String, message msg:String) -> String
    func write(msg: String) -> Void
    
    var level:LogLevel { get set }
}

public class AbstractLogAppender: LogAppenderType {
    public var name:String {
        return "AbstractLogAppender"
    }
    
    let dateFormatter: NSDateFormatter
    public var level:LogLevel
    
    public func format(category cat:String, levelName name: String, message msg: String) -> String {
        let dt = dateFormatter.stringFromDate( NSDate() )
        let str = "\( dt ) \( cat ) \( name ) \( msg )"
        
        return str
    }
    
    public func write(msg:String) {
        // noop
    }
    
    public init(level:LogLevel? = .Debug, dateFormat:String? = "HH:mm:ss.SSS") {
        self.level = level!
        
        self.dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = dateFormat!
    }
}

public final class ConsoleLogAppender: AbstractLogAppender {
    override public var name:String {
        return "ConsoleLogAppender"
    }
    
    override public func write(msg:String) {
        print( msg )
    }
}

public final class NSLogAppender: LogAppenderType {
    public var name:String {
        return "NSLogAppender"
    }
    
    public var level:LogLevel
    
    public func format(category cat:String, levelName name: String, message msg: String) -> String {
        let str = "\( cat ) \( name ) \( msg )"
        
        return str
    }
    
    public func write(msg:String) {
        NSLog( msg )
    }
    
    public init(level:LogLevel? = .Debug) {
        self.level = level!
    }
}

public final class MockLogAppender: AbstractLogAppender {
    override public var name:String {
        return "MockLogAppender"
    }
    
    public var messages = [String]()
    public func clear() {
        messages.removeAll()
    }
    
    override public func write(msg: String) {
        self.messages.append( msg )
    }
}

/**
    SimpleLogger: a centralized log manager would insure that appenders were created once
*/

public enum LogLevel: Int {
    case Trace, Debug, Info, Warn, Error, Off
}

public protocol Logger {
    var category:String { get }
    var isDebug:Bool { get }
    func debug(msg:String)
    func info(msg:String)
    func warn(msg:String)
    func error(msg:String)
    var level:LogLevel { get set }
    var appenderList:[ LogAppenderType ] { get }
    func findAppenderByName(name: String) -> LogAppenderType?
}

public class SimpleLogger: Logger {
    private var appenders:[ LogAppenderType ]
    final public let category: String
    final public var level: LogLevel
    
    private func log(levelName:String, _ msg:String) {
        // loop through the log appenders and send out...
        for appender in appenders {
            if appender.level.rawValue <= self.level.rawValue {
                appender.write( appender.format( category:category, levelName:levelName, message:msg ))
            }
        }
    }
    
    final public var isDebug:Bool {
        return level.rawValue <= LogLevel.Debug.rawValue
    }
    
    final public func debug(msg: String) {
        if isDebug {
            log("debug", msg)
        }
    }
    
    final public func info(msg: String) {
        if level.rawValue <= LogLevel.Info.rawValue {
            log("info ", msg)
        }
    }
    
    final public func warn(msg: String) {
        if level.rawValue <= LogLevel.Warn.rawValue {
            log("warn ", msg)
        }
    }
    
    final public func error(msg: String) {
        log("error", msg)
    }
    
    final public var appenderList:[ LogAppenderType ] {
        return appenders
    }
    
    final public func findAppenderByName(name: String) -> LogAppenderType? {
        for appender in appenders {
            if appender.name == name {
                return appender
            }
        }
        
        return nil
    }
    
    public convenience init(category: String) {
        let lvl = LogLevel.Info
        let apps = [ ConsoleLogAppender(level: lvl) as LogAppenderType ]
        self.init(category: category, level: lvl, appenders: apps )
    }
    
    public convenience init(category: String, level:LogLevel) {
        let app = [ ConsoleLogAppender(level: level) as LogAppenderType ]
        self.init(category: category, level: level, appenders: app)
    }
    
    /**
    create a default info level logger with console appender; appender is created at the same level
    */
    public init(category: String, level:LogLevel, appenders:[LogAppenderType]) {
        self.category = category
        
        self.level = level
        
        self.appenders = appenders
    }
}
