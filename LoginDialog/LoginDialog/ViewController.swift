//
//  ViewController.swift
//  LoginDialog
//
//  Created by darryl west on 6/29/15.
//  Copyright © 2015 darryl west. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    // set the factory dependent objects
    let log = ApplicationFactory.sharedInstance.createLogger("ViewController")
    let loginValidator = ApplicationFactory.sharedInstance.loginValidator
    let loginService = ApplicationFactory.sharedInstance.loginService
    
    // TODO : implement simple UI with username + password


}

