//
//  User.swift
//  LoginDialog
//
//  Created by darryl west on 6/29/15.
//  Copyright © 2015 darryl west. All rights reserved.
//

import Foundation

public struct User {
    public let id:String
    public let username:String
    public let session:String
    
    public init(id:String, username:String, session:String) {
        self.id = id
        self.username = username
        self.session = session
    }
    
}