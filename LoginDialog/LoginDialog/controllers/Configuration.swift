//
//  Configuration.swift
//  LoginDialog
//
//  Created by darryl west on 6/29/15.
//  Copyright © 2015 darryl west. All rights reserved.
//

import Foundation

public enum Environment:String, CustomStringConvertible {
    case Development = "development"
    case Test = "test"
    case Staging = "staging"
    case Production = "production"
    
    public var description:String {
        return self.rawValue
    }
}

public struct Configuration {
    
    public let appVersion:String
    
    public let environment:Environment
    public let logAppenders:[ LogAppenderType ]
    
    public let remoteDataHost:String
    public let dataService:String
    
    public let usernameLengthRange:MinMaxRange
    public let passwordLengthRange:MinMaxRange
    
    public func createLogger(category:String, level:LogLevel? = .Info) -> Logger {
        let logger = SimpleLogger(category: category, level: level!, appenders: self.logAppenders )
        
        return logger;
    }
    
    public init(environment:Environment) {
        let vers = "0.1.13"
        
        self.environment = environment
        
        self.usernameLengthRange = MinMaxRange(min:6, max:50)
        self.passwordLengthRange = MinMaxRange(min:4, max:30)
        
        self.dataService = "/AuthenticationService"
        
        /// init based on environment
        switch self.environment {
        case .Production, .Staging:
            appVersion = vers
            // TODO: replace with a rolling file appender
            logAppenders = [ ConsoleLogAppender( level: .Info ) ]
            
            remoteDataHost = "http://app.roundpeg.com"
        case .Test:
            appVersion = vers + "-test"
            remoteDataHost = "http://test.roundpeg.com"
            logAppenders = [ MockLogAppender( level: .Debug ), ConsoleLogAppender( level: .Info ) ]
        case .Development:
            appVersion = "\( vers )-\( NSDate().timeIntervalSinceReferenceDate )"
            logAppenders = [ ConsoleLogAppender( level: .Debug ) ]
            remoteDataHost = "http://dev.roundpeg.com"
        }
    }
}