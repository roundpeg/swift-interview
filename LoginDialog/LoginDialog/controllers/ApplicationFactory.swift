//
//  ApplicationFactory.swift
//  LoginDialog
//
//  Created by darryl west on 6/29/15.
//  Copyright © 2015 darryl west. All rights reserved.
//

import Foundation

/**
    ApplicationFactory: a factory that builds application objects, delegates, services, etc.

    Used as a sharedApplication at run-time and prototype for tests (see MockApplicationFactory)
*/
public class ApplicationFactory {
    
    let config:Configuration
    let log:Logger
    
    public private(set) var loginValidator:LoginValidator
    public private(set) var loginService:LoginService
    
    public func createLogger(category: String, level: LogLevel? = .Info) -> Logger {
        return config.createLogger( category, level: level )
    }
    
    public var environment:Environment {
        return config.environment
    }
    
    public var version:String {
        return config.appVersion
    }
    
    private convenience init() {
        self.init(conf: Configuration( environment: .Production ))
    }
    
    public init(conf:Configuration) {
        self.config = conf
        
        self.log = conf.createLogger("ApplicationFactory", level:.Info )
        
        self.loginValidator = LoginValidator( configuration: self.config )
        self.loginService = LoginService( configuration: self.config )
        
        log.info( "application factory is ready, env: \( self.config.environment ), version: \( self.config.appVersion )" )
    }
    
    public class var sharedInstance: ApplicationFactory {
        
        struct Static {
            static let instance:ApplicationFactory = ApplicationFactory( )
        }
        
        return Static.instance
    }
}

