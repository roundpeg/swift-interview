//
//  LoginService.swift
//  LoginDialog
//
//  Created by darryl west on 6/29/15.
//  Copyright © 2015 darryl west. All rights reserved.
//

import Foundation

public enum ResponseCallback<T1: Any, T2: ErrorType> {
    case Ok(T1)
    case Fail(T2)
}

public struct LoginRequest {
    public let username:String
    public let password:String
    
    public init(username:String, password:String) {
        self.username = username
        
        // TODO: salt and hash this first
        self.password = password
    }
}

/// this is more of  mock at this point; but, it does use a separate threat to invoke the callback, so it's a good mock

public class LoginService {
    let log:Logger
    let dataHost:String
    
    public func sendLoginRequest(request:LoginRequest, callback:(ResponseCallback<User, NSError>) -> Void ) {
        log.info("send login request: \( request )")
        
        let queue = dispatch_get_global_queue( DISPATCH_QUEUE_PRIORITY_DEFAULT, 0 )
        
        dispatch_async(queue, {
            let log = self.log
            // when complete, invoke the callback
            log.info("process user login in background thread, request: \( request )")
            
            if request.password == "failfailfail" {
                let error = NSError(domain: "loginService", code: 10, userInfo: nil)
                log.warn("login failed \( error )")
                callback( ResponseCallback.Fail( error ))
            } else {
                let user = User(id: "2344", username: request.username, session: NSUUID().UUIDString.lowercaseString)
                callback( ResponseCallback.Ok( user ))
            }
        })
    }
    
    init(configuration:Configuration) {
        self.log = configuration.createLogger("LoginService")
        self.dataHost = configuration.remoteDataHost
    }
}