//
//  LoginValidatorTests.swift
//  LoginDialog
//
//  Created by darryl west on 6/29/15.
//  Copyright © 2015 darryl west. All rights reserved.
//

import XCTest
@testable import LoginDialog

class LoginValidatorTests: XCTestCase {
    
    let factory = MockApplicationFactory.mockInstance
    
    func testInstance() {
        let validator = factory.loginValidator
        
        XCTAssertNotNil(validator, "validator should not be null")
    }
    
    func testValidUserName() {
        let validator = factory.loginValidator
        let username = "testuser@test.com"
        
        XCTAssertTrue( validator.isUsernameValid( username ), "user name should be valid")
        
    }
    
    func testInvalidUserName() {
        let validator = factory.loginValidator
        let username = "fds"
        
        XCTAssertFalse( validator.isUsernameValid( username ), "user name should not be valid")
        
    }
    
    func testValidPassword() {
        let validator = factory.loginValidator
        
        let password = "12345567"
        
        XCTAssertTrue( validator.isPasswordValid( password ), "password should be valid")
        
    }
    
    func testMinMaxIntRange() {
        let range = MinMaxRange( min: 10, max: 100 )
        
        XCTAssert( range.isValid( 55 ), "should be in the range")
        // test the bounds
        XCTAssert( range.isValid( 10 ), "should be in the range")
        XCTAssert( range.isValid( 100 ), "should be in the range")
        
        // test the fails
        XCTAssertFalse( range.isValid( 9 ), "should be too small")
        XCTAssertFalse(range.isValid( 101 ), "should be too big")
    }
    
    func testMinMaxStringRange() {
        let range = MinMaxRange( min: 2, max: 10 )
        
        XCTAssert( range.isValid( "my string" ), "should be in the range")
        // test the bounds
        XCTAssert( range.isValid( "my" ), "should be in the range")
        XCTAssert( range.isValid( "1234567890" ), "should be in the range")
        
        // test the fails
        XCTAssertFalse( range.isValid( "x" ), "should be too small")
        XCTAssertFalse(range.isValid( "a way long string thing" ), "should be too big")
    }
    
}

