//
//  UserDataset.swift
//  LoginDialog
//
//  Created by darryl west on 6/29/15.
//  Copyright © 2015 darryl west. All rights reserved.
//

import Foundation

class UserDataset {
    let goodLogin = (name:"test@test.com", password:"flarberbaker")
    let badLogin = (name:"fail@fail.com", password:"failfailfail")
}
