//
//  ApplicationFactoryTests.swift
//  LoginDialog
//
//  Created by darryl west on 6/29/15.
//  Copyright © 2015 darryl west. All rights reserved.
//

import XCTest
@testable import LoginDialog

class ApplicationFactoryTests: XCTestCase {
    
    func testInstance() {
        // This is an example of a functional test case.
        let factory = ApplicationFactory.sharedInstance
        
        XCTAssertEqual(factory.environment, Environment.Production, "should default to production")
        XCTAssertNotNil(factory.version, "version should have something")
    }
    
    func testDevelopmentInstance() {
        let factory = ApplicationFactory(conf: Configuration(environment: .Development))
        
        XCTAssertEqual(factory.environment, Environment.Development, "should be dev env")
    }
    
    func testTestInstance() {
        let factory = MockApplicationFactory.mockInstance
        
        XCTAssertEqual(factory.environment, Environment.Test, "should be test env")
    }
    
    func testCreateLogger() {
        let factory = MockApplicationFactory.mockInstance
        
        XCTAssertEqual(factory.environment, Environment.Test, "should be test environment")
        
        let log = factory.createLogger("TestLogger")
        
        let appenders = log.appenderList
        
        XCTAssertEqual(appenders.count, 2, "should be 2 appenders in the test environment")
        
        if let mockAppender:MockLogAppender? = log.findAppenderByName( "MockLogAppender" ) as? MockLogAppender {
            let appender = mockAppender!
            var messageCount = appender.messages.count
            
            XCTAssertEqual(appender.level, LogLevel.Debug, "should be at debug level")
            
            log.info("test message")
            messageCount++
            
            XCTAssertEqual(appender.messages.count, messageCount, "should have one more message")
        } else {
            XCTFail("mock appender is nil")
        }
    }
}
