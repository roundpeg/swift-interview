//
//  ConfigurationTests.swift
//  LoginDialog
//
//  Created by darryl west on 6/29/15.
//  Copyright © 2015 darryl west. All rights reserved.
//

import XCTest
@testable import LoginDialog

class ConfigurationTests: XCTestCase {
    
    func testInstance() {
        let config = Configuration(environment: .Test)
        
        XCTAssertEqual(config.environment, .Test, "should be production environment")
        XCTAssertTrue(config.usernameLengthRange.min > 0, "should be greater than a minimum")
        XCTAssertTrue(config.usernameLengthRange.max < 100, "should be less than a maximum")
        
        XCTAssertTrue(config.passwordLengthRange.min > 0, "should be greater than a minimum")
        XCTAssertTrue(config.passwordLengthRange.max < 100, "should be less than a maximum")
        
        XCTAssertEqual(config.logAppenders.count, 2, "should be 2 appenders")
        XCTAssertEqual(config.remoteDataHost, "http://test.roundpeg.com", "should be test remote url")
        XCTAssertEqual(config.dataService, "/AuthenticationService", "should be remote data service")
    }
    
    func testDevInstance() {
        let config = Configuration(environment: .Development)
        
        XCTAssertEqual(config.environment, .Development, "should be production environment")
        XCTAssertEqual(config.logAppenders.count, 1, "should be 1 appender")
        XCTAssertEqual(config.remoteDataHost, "http://dev.roundpeg.com", "should be dev remote url")
        XCTAssertEqual(config.dataService, "/AuthenticationService", "should be remote data service")
    }
    
    func testProductionInstance() {
        let config = Configuration(environment: .Production)
        
        XCTAssertEqual(config.environment, .Production, "should be production environment")
        XCTAssertEqual(config.logAppenders.count, 1, "should be 1 appender")
        XCTAssertEqual(config.remoteDataHost, "http://app.roundpeg.com", "should be production remote url")
        XCTAssertEqual(config.dataService, "/AuthenticationService", "should be remote data service")
    }
    
    func testStagingInstance() {
        let config = Configuration(environment:.Staging)
        
        XCTAssertEqual(config.environment, .Staging, "should be staging environment")
        XCTAssertEqual(config.logAppenders.count, 1, "should be 1 appender")
        XCTAssertEqual(config.remoteDataHost, "http://app.roundpeg.com", "should be production remote url")
        XCTAssertEqual(config.dataService, "/AuthenticationService", "should be remote data service")
    }
    
    
    
}
