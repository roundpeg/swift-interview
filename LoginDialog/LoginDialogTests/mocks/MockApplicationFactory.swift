//
//  MockApplicationFactory.swift
//  LoginDialog
//
//  Created by darryl west on 6/29/15.
//  Copyright © 2015 darryl west. All rights reserved.
//

import Foundation
@testable import LoginDialog

class MockApplicationFactory {
    class var mockInstance: ApplicationFactory {
        let config = Configuration(environment: .Test )
        return ApplicationFactory( conf:config )
    }
}
