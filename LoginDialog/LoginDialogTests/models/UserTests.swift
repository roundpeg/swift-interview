//
//  UserTests.swift
//  LoginDialog
//
//  Created by darryl west on 6/29/15.
//  Copyright © 2015 darryl west. All rights reserved.
//

import XCTest
@testable import LoginDialog

class UserTests: XCTestCase {
    
    func testInstance() {
        let id = NSUUID().UUIDString.lowercaseString
        let username = "My Test Name"
        let session = NSUUID().UUIDString.lowercaseString
        
        let user = User(id:id, username:username, session:session)
        
        XCTAssertEqual(user.id, id, "id should match")
        XCTAssertEqual(user.username, username, "name should match")
        XCTAssertEqual(user.session, session, "session should match")
    }
}
