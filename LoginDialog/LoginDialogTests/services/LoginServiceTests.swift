//
//  LoginServiceTests.swift
//  LoginDialog
//
//  Created by darryl west on 6/29/15.
//  Copyright © 2015 darryl west. All rights reserved.
//

import XCTest
@testable import LoginDialog

class LoginServiceTests: XCTestCase {
    
    let factory = MockApplicationFactory.mockInstance
    let dataset = UserDataset()
    
    // for async tests
    let timeout = NSTimeInterval( 2.0 )
    let expectationName = "loginRequestComplete"
    
    func testInstance() {
        let service = factory.loginService
        
        XCTAssertNotNil(service, "service should not be nil")
    }
    
    func testSendLoginRequest() {
        let completedExpectation = expectationWithDescription( expectationName )
        let service = factory.loginService
        let request = LoginRequest(username: dataset.goodLogin.name, password:dataset.goodLogin.password )
        
        func callback(response: ResponseCallback<User, NSError>) -> Void {
            
            switch response {
            case .Ok(let user):
                print( "user: \( user )")
                
                XCTAssertNotNil(user.id, "id should not be null")
                XCTAssertNotNil(user.username, "username should not be null")
                XCTAssertNotNil(user.session, "user session should not be null")
            case .Fail(let err):
                XCTFail("error should be nil but was \( err )")
            }

            completedExpectation.fulfill()
        }
        
        service.sendLoginRequest(request, callback: callback);
        
        // this blocks until timeout or expectation is fulfilled
        waitForExpectationsWithTimeout(timeout, handler: { error in
            XCTAssertNil(error, "login request should not timeout")
        })
        
    }
    
    func testSendLoginRequestFail() {
        let completedExpectation = expectationWithDescription( expectationName )
        let service = factory.loginService
        let request = LoginRequest(username: dataset.badLogin.name, password:dataset.badLogin.password )
        
        func callback(response: ResponseCallback<User, NSError>) -> Void {
            
            switch response {
            case .Ok(let user):
                print( "user: \( user )")
                XCTFail("user should be nil: \( user )")
            case .Fail(let err):
                print( "error: \( err )")
                XCTAssertNotNil("error should not be nil: \( err )")
            }
                        
            completedExpectation.fulfill()
        }
        
        service.sendLoginRequest(request, callback: callback);
        
        // this blocks until timeout or expectation is fulfilled
        waitForExpectationsWithTimeout(timeout, handler: { error in
            XCTAssertNil(error, "login request should not timeout")
        })
        
    }
    
}

